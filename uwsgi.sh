#!/bin/bash
.virtualenv/bin/uwsgi \
	--http 127.0.0.1:5001 \
	--module pyindieweb:app \
	--processes 4 \
	--threads 2 \
	--stats 127.0.0.1:9191 \
	--virtualenv .virtualenv \
	--offload-threads 2 \
	--cache2 name=hcards,items=150,purge_lru=1
