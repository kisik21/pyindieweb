import mf2py
from flask import request, jsonify
from pyindieweb import app

actions = dict()

@app.route('/api/microsub', methods=['GET', 'POST'])
def microsub():
    data = request.form or request.args
    try:
        action = data["action"]
    except:
        return "Bad Request", 400
    actions[action](data)
