import os
import random
import string
from urllib.parse import urlencode
import requests
import mf2py
from flask import json, jsonify, abort, redirect, session, request
from pyindieweb import app, render_template

STATES = dict()

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/api/indielogin', methods=['GET', 'POST'])
def indielogin():
    try:
        url = request.form.get("user")
        red = request.form.get("redirect_to", request.scheme + "://" + request.host + "/")
        page = mf2py.parse(url=url)
        endpoint = page["rels"]["authorization_endpoint"][0]
        state = ''.join(random.choice(string.ascii_lowercase) for i in range(16))
        # XXX make this not hardcoded
        if not os.path.exists('/run/pyindieblog/states'):
            os.makedirs('/run/pyindieblog/states')
        with open('/run/pyindieblog/states/{}.json'.format(state), 'w') as f:
            json.dump({"auth": endpoint, "me": url, "redirect": red}, f)
        return redirect(endpoint + "?" + urlencode({
            'me': url,
            'client_id': request.scheme + "://" + request.host,
            'redirect_uri': request.scheme + "://" + request.host + "/api/indielogin/callback",
            'state': state,
            'response_type': 'id',
        }))
    except Exception as e:
        app.logger.exception(e)
        abort(400, e)

@app.route('/api/indielogin/callback')
def callback():
    try:
        code = request.args["code"]
        state = request.args.get("state")
        with open('/run/pyindieblog/states/{}.json'.format(state)) as f:
            statefile = json.load(f)
        os.remove('/run/pyindieblog/states/{}.json'.format(state))
        me = requests.post(statefile['auth'], headers={"Accept": "application/json"}, data={'code': code, 'redirect_uri': request.base_url, 'client_id': request.scheme + "://" + request.host}).json()["me"]
        session["me"] = me
        session.permanent = True
        return redirect(statefile['redirect'])
    except Exception as e:
        app.logger.exception(e)
        abort(400, e)
