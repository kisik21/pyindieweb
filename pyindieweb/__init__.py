import os
import logging
import json
logging.basicConfig(level=logging.DEBUG)

import flask
from flask import request, send_from_directory, safe_join, send_file, abort, json, jsonify, session
from jinja2 import FileSystemLoader
from flask.helpers import locked_cached_property
from flaskext.markdown import Markdown

class Flask(flask.Flask):
    @locked_cached_property
    def jinja_loader(self):
        """The Jinja loader for this package bound object.

        .. versionadded:: 0.5
        """
        if self.template_folder is not None:
            return FileSystemLoader(os.path.join(self.root_path,
                                                 self.template_folder),
                                    followlinks=True)

instance_path = os.path.join(os.getcwd(), 'instance')
# create and configure the app
app = Flask(
    __name__,
    instance_relative_config=True,
    instance_path = instance_path,
    template_folder = os.path.join(instance_path, "templates"),
    static_folder = os.path.join(instance_path, "static")
)
md = Markdown(app)
app.config.from_mapping(
    BLOGS = {
        "example.com": {},
    },
    TOKENS = {
        "telegraph":  "dev",
        "twitter": {
            "key": "dev",
            "secret": "dev"
        }
    },
    TOKEN_ENDPOINT = "{}/api/indieauth/tokens".format("https://{}".format("fireburn.ru")),
    SECRET_KEY = 'dev',
    SYNDICATE = {
        "Twitter (via brid.gy)": "https://brid.gy/publish/twitter",
        "IndieNews (en)": "https://news.indieweb.org/en"
    },
    SYNC=False
)

# ensure the instance folder exists
try:
    os.makedirs(app.instance_path)
except OSError:
    pass
app.config.from_pyfile('config.py', silent=True)

# A wrapper function
def render_template(template, *args, **kwargs):
    """Return template based on host that is used to get a resource."""
    template = os.path.join(request.host, template)
    return flask.render_template(template, site=app.config["BLOGS"][request.host], session=session, *args, **kwargs)

@app.route('/helloworld')
def hello():
    return render_template("hello.html")

import pyindieweb.posts
@app.route('/<path:filename>')
def get_custom_page(filename):
    path = safe_join(app.instance_path, request.host, 'pages', filename)
    try:
        if os.path.isdir(path):
            path = safe_join(path, "index")
        elif os.path.isfile(path):
            return send_file(path)
        # If it is not file nor a directory, find a markdown file with that name and render it!
        with open(safe_join(path + ".md")) as f:
            try:
                with open(safe_join(path + ".json")) as j:
                    meta = json.read(j)
            except FileNotFoundError:
                meta = {"name": os.path.basename(path).title()}
            return render_template("single.html", page=f.read(), meta={})
    except FileNotFoundError:
        abort(404, "There's no page here like that!")
    

@app.route('/blogroll')
def blogroll():
    try:
        with open(os.path.join(app.instance_path, request.host, 'blogroll.json')) as f:
            blogroll = json.load(f)
        return render_template("blogroll.html", blogroll=blogroll)
    except FileNotFoundError:
        abort(404, "This user's blogroll is empty.")

@app.route('/favicon.ico')
def favicon():
    return "", 301, {"Location": "/resources/favicon.ico"}

@app.route('/uploads/<path:filename>')
def uploads(filename):
    try:
        return send_from_directory(os.path.join(app.instance_path, request.host, 'uploads'), filename)
    except FileNotFoundError:
        abort(404)

# /resources/ serves static files for /static/{{request.host}}
# This gets us cleaner URLs in templates
@app.route('/resources/<path:filename>')
def resources(filename):
    try:
        return send_from_directory(os.path.join(app.static_folder, request.host), filename)
    except FileNotFoundError:
        abort(404)

@app.errorhandler(410)
def gone(e):
    return render_template("single.html", page="# HTTP 410 Gone\n{}\n\nMaybe you'd wanna go to my [home](/) page?".format(e), meta={"name": "HTTP 410 Gone"})

@app.errorhandler(404)
def notfound(e):
    return render_template("single.html", page="# HTTP 404 Not Found\n{}\n\nMaybe you'd wanna go to my [home](/) page?".format(e), meta={"name": "HTTP 404 Not Found"})

@app.errorhandler(451)
def notfound(e):
    return render_template("single.html", page="# HTTP 451 Unavailable for legal reasons\n{}\n\nMaybe you'd wanna go to my [home](/) page?".format(e), meta={"name": "HTTP 451 Unavailable for legal reasons"})

@app.route('/451')
@app.route('/1984')
def farhenheit():
    abort(451, "<br>Freedom is slavery,<br>War is peace,<br>Ignorance is bliss.\n\nThere was never such a page. Its author never existed. You **HAVEN'T SEEN ANYTHING THERE. THIS PAGE DOESN'T EXIST.**")

import pyindieweb.micropub
import pyindieweb.indieauth
import pyindieweb.indielogin
